
public class Card{
	
	private String suit;
	private String value;
	
	//constructor
	public Card(String value, String suit){
		
		this.suit = suit;
		this.value = value;
	}
	
	//getters
	public String getSuit(){
		return this.suit;
	}
	
	public String getValue(){
		return this.value;
	}
	
	//toString
	public String toString(){
		return this.value + " of " + this.suit;
	}
	
}