
public class GameManager{
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	//constructor
	public GameManager(){
		//create deck
		this.drawPile = new Deck();
		
		//draw Center + Player cards
		this.drawPile.shuffle();
		this.centerCard = this.drawPile.drawTopCard();
		this.playerCard = this.drawPile.drawTopCard();
	}
	
	//getters
	
	//toString
	public String toString(){
		return "\nThe CENTER Card is: " + this.centerCard + "\nThe PLAYER Card is: " + this.playerCard;
	}
	
	//instance methods
	public void dealCards(){
		////SHUFFLE then draw Center + Player cards
		this.drawPile.shuffle();
		this.centerCard = this.drawPile.drawTopCard();
		this.playerCard = this.drawPile.drawTopCard();
	}
	
	public int getNumbersOfCards(){
		return this.drawPile.length();
	}
	
	public int calculatePoints(){
		
		if(this.centerCard.getValue().equals(this.playerCard.getValue()) ){
			return 4;
		}
		else if(this.centerCard.getSuit().equals(this.playerCard.getSuit()) ){
			return 2;
		}
		else{
			return -1;
		}
	}
}