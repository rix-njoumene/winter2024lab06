import java.util.Scanner;

public class LuckyCardGameApp{
	
	public static void main(String[] args){
		
		GameManager manager = new GameManager();
		int totalPoints = 0;
		
		//Welcoming
		System.out.println("\nWelcome User in this Game of Cards!");
		System.out.println("Now, let the Game Start!");
		
		//Game Loop
		while(manager.getNumbersOfCards() > 1 && totalPoints < 5){
			
			//display drawn Center + Player cards
			System.out.println(manager);
			
			//Display and calculate POINTS
			totalPoints += manager.calculatePoints();
			
			if(manager.calculatePoints() > 0){
				System.out.println("That's a RIGHT MATCH!");
			}
			else{
				System.out.println("That's a WRONG MATCH!");
			}
			System.out.println("\nYou now have a TOTAL POINTS OF " + totalPoints + " POINTS");
			
			//Shuffle + Draw Again
			manager.dealCards();
		}
		
		System.out.println("Your FINAL SCORE is : " + totalPoints);
		if(totalPoints >= 5){
			System.out.println("Bravo, You WON the Game! \nYou were quite Lucky, I have to say...");
		}
		else{
			System.out.println("Too Bad, You LOST the Game! \nIt seems like Luck wasn't with you today...");
		}
		
		
	}
	
}